﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

// From FPSCamera mod

namespace SilenceObnoxiousSirens {
    public class Debugger {
        private static Debugger instance = null;
        public DebugConsole console = null;
        public GameObject gameObject = null;

        public static void Initialize() {
            instance = new Debugger();
        }

        public static void Log(string s) {
            if (instance != null) {
                instance.LogInternal(s);
            }
        }

        public static void ClearLog() {
            instance.ClearLogInternal();
        }

        public void LogInternal(string s) {
            if (console == null) { 
                gameObject = new  GameObject();
                console = gameObject.AddComponent<DebugConsole>();
                console.debugger = this;
            } 
            console.Log(s);
        }

        public void ClearLogInternal() { 
            if (console != null) { 
                console.text = "";
            }
        }

    }


    public class DebugConsole : MonoBehaviour {
        public Debugger debugger;
        private Rect windowRect = new Rect(16, 16, 380, 500);
        private Vector2 scrollPosition = Vector2.zero;
        public string text = "";
        private bool showConsole = true;

        private void OnGUI() {
            if (showConsole) {
                windowRect = GUI.Window(12522, windowRect, MainWindowFunc, "Debug Console");
            }
        }

        private void MainWindowFunc(int windowID) {
            GUI.DragWindow();
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            GUILayout.Label(text);
            GUILayout.EndScrollView();
        }

        public void Log(string s) {
            text = String.Format("{0} * {1}\n", text, s);
        }

        void OnDestroy() {
            debugger.console = null;
        }

        void Update() {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C)) {
                showConsole = !showConsole;
                if (showConsole) {
                    debugger.LogInternal("Debug console opened");
                }
            }
        }
    }
}
