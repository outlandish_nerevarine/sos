﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework;

namespace SilenceObnoxiousSirens {
    class Silencer : MonoBehaviour {

        public static Silencer instance;
        public static GameObject owner;

        UInt64 frameCount = 0;

        public static void Initialize() { 
            owner = new GameObject();
            owner.name = Mod.MOD_NAME + "_owner";
            instance = owner.AddComponent<Silencer>();
            Debugger.Log("Init complete");
        }
        
        public static void Shutdown() { 
            Debugger.Log("Shutdown!");
            if (owner) { 
                Destroy(owner); 
            }
            if (instance) { 
                Destroy(instance);
            }
        }

        void Update() { 
            if (frameCount >= 60) { 
                Debugger.Log("Attempting to silence...");
                frameCount = 0;
                if (tryToSilence()) { 
                    Debugger.Log("Silence successful!");
                    gameObject.SetActive(false);
                }
            }
            frameCount++;
        }

        /**
         * This could fail if there are no ambulances or police cars yet.
         **/
        private bool tryToSilence() { 
            bool succ = false;
            VehicleManager vm = Singleton<VehicleManager>.instance;
            Vehicle[] vehicles = vm.m_vehicles.m_buffer;
            for (int x=0; x<vehicles.Length; ++x) { 
                // Harcoded strings. Works on 4/7/2015
                if ( vehicles[x].Info.name.ToLower() == "ambulance" || vehicles[x].Info.name.ToLower() == "police car" )  { 
                    VehicleInfo.Effect[] effects = vehicles[x].Info.m_effects;

                    for (int i=0; i<effects.Length; ++i) { 
                        if (effects[i].m_effect.name.Contains("Emergency")) { 
                            MultiEffect me = effects[i].m_effect.GetComponent<MultiEffect>();
                            MultiEffect.SubEffect[] meese = me.m_effects;
                            for (int ii=0; ii<meese.Length; ++ii) { 
                                if (meese[ii].m_effect.name.Contains("Siren")) { 
                                    SoundEffect se = meese[ii].m_effect.GetComponent<SoundEffect>();
                                    AudioInfo audioInfo = null;
                                    if (se != null) { 
                                        audioInfo = se.m_audioInfo;
                                    }

                                    if (audioInfo != null) { 
                                        se.m_range = 0;
                                        audioInfo.m_volume = 0;
                                        succ = true;
                                        break;
                                    }

                                }

                            }
                        }
                    } // for
                } // if
                if (succ) break;
            } // for
            
            Debugger.Log("Trying to silence...");

            if (!succ) { 
                Debugger.Log("Failure");
            } else { 
                Debugger.Log("Success!");
            }

            return succ; 
        }
    }
}
