﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICities;
using ColossalFramework;

namespace SilenceObnoxiousSirens {
    public class Mod : IUserMod {
        public const string MOD_NAME = "Silence Obnoxious Sirens";
        public const string MOD_VERSION = "0.1";

        public string Name { 
            get { return MOD_NAME + " " + MOD_VERSION; }
        }

        public string Description { 
            get { return "Disables those obnoxiously loud sirens!"; } 
        }

    }

    public class ModLoader : LoadingExtensionBase {
        public override void OnLevelLoaded(LoadMode mode) {
            //Debugger.Initialize();
            //Debugger.Log("Started up");
            Silencer.Initialize();
        }

        public override void OnLevelUnloading() {
            Silencer.Shutdown();
        }

    }
}
